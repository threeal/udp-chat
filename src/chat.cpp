#include "chat.h"
#include "udp.h"

#include <sstream>
#include <unistd.h>

Chat::Chat(int argc, char **argv)
{
  if (argc >= 2)
  {
    user_name_ = argv[1];

    win_ = initscr();
    timeout(0);

    updateTerminalSize();
    printMessages();
    printUserInput();
    refresh();

    udp_ = new UDP(9898);

    process();
  }
}

Chat::~Chat()
{
  delete udp_;
}

void Chat::addMessage(std::string message)
{
  messages_.push_back(message);
  printMessages();
}

void Chat::process()
{
  int i = 0;
  while (true)
  {
    updateTerminalSize();

    while (udp_->isReceiveMessage())
    {
      addMessage(udp_->getReceivedMessage());
    }

    char ch = getch();
    if (ch > 0)
    {
      switch (ch)
      {
      case '\n':
        user_input_ = user_name_ + " : " + user_input_;
        addMessage(user_input_);
        udp_->sendMessage(user_input_);
        user_input_ = "";
        break;

      case 127:
        if (user_input_.length() > 0)
          user_input_.resize(user_input_.size() - 1);
        break;

      default:
        if (user_input_.length() < 64)
          user_input_ = user_input_ + ch;
        break;
      }
    }

    printUserInput();

    usleep(8000);
  }
}

void Chat::updateTerminalSize()
{
  getmaxyx(win_, height_, width_);
}

void Chat::printMessages()
{
  clear();

  for (int i = 0; (i < messages_.size() && i < height_ - 2); i++)
  {
    std::string message = messages_[messages_.size() - 1 - i];
    mvwprintw(win_, height_ - 3 - i, 0, "%s", message.c_str());
  }

  refresh();
}

void Chat::printUserInput()
{
  for (int i = 0; i < width_; i++)
  {
    mvwprintw(win_, height_ - 1, i, " ");
  }

  mvwprintw(win_, height_ - 1, 0, "%s : %s", user_name_.c_str(), user_input_.c_str());
}