#include "udp.h"

#include <arpa/inet.h>
#include <fcntl.h>
#include <functional>
#include <ifaddrs.h>
#include <sys/socket.h>
#include <unistd.h>

UDP::UDP(int port)
{
  port_ = port;
  socket_ = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

  int broadcast = 1;
	setsockopt(socket_, SOL_SOCKET, SO_BROADCAST, (void *) &broadcast, sizeof(broadcast));

	int fcntl_flags = fcntl(socket_, F_GETFL, 0);
	fcntl(socket_, F_SETFL, fcntl_flags | O_NONBLOCK);

	struct sockaddr_in sa = { 0 };
	sa.sin_family      = AF_INET;
	sa.sin_addr.s_addr = htonl(INADDR_ANY);
	sa.sin_port        = htons(port_);

  bind(socket_, (struct sockaddr *) &sa, sizeof(sa));

  std::function<void()> method = [this]()
  {
    while (true)
    {
      process();
    }
  };

  thread_ = new std::thread(method);
}

UDP::~UDP()
{
  if (thread_ != nullptr)
  {
    if (thread_->joinable())
      thread_->join();
  }

  if (socket_ > 0)
    close(socket_);
}

void UDP::process()
{
  if (socket_ < 0)
    return;

  struct sockaddr_in remote_address;
  socklen_t address_len = sizeof(remote_address);

  char buffer[128];
  if (recvfrom(socket_, buffer, 128, 0, (struct sockaddr *)&remote_address, &address_len) > 0)
    received_.push_back(buffer);

  while (sent_.size() > 0)
  {
    std::string message = sent_.front();
    sent_.pop_front();

    struct ifaddrs *ifap;
    getifaddrs(&ifap);

    for (struct ifaddrs *p = ifap; p != NULL; p = p->ifa_next)
    {
      if (p->ifa_addr == NULL)
        continue;

      if (p->ifa_addr->sa_family != AF_INET)
        continue;

      uint32_t interface_addr = ntohl(((struct sockaddr_in *)(p->ifa_addr))->sin_addr.s_addr);
      uint32_t broadcast_addr = ((struct sockaddr_in *)(p->ifa_broadaddr))->sin_addr.s_addr;

      if (interface_addr > 0 && interface_addr != 0x7F000001)
      {
        struct sockaddr_in recipient = { 0 };
        recipient.sin_family      = AF_INET;
        recipient.sin_port        = htons(port_);
        recipient.sin_addr.s_addr = broadcast_addr;

        sendto(socket_, message.c_str(), 128, 0, (const struct sockaddr*)&recipient, sizeof recipient);
      }
    }

    freeifaddrs(ifap);
  }
}