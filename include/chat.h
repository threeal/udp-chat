#ifndef UDP_CHAT_CHAT_H_
#define UDP_CHAT_CHAT_H_

#include <curses.h>
#include <string>
#include <vector>

class UDP;

class Chat
{
public:

  Chat(int argc, char **argv);
  ~Chat();

  void addMessage(std::string message);

private:

  UDP *udp_;

  WINDOW *win_ = nullptr;

  std::vector<std::string> messages_ = { };

  std::string user_name_ = "user";
  std::string user_input_ = "";

  int width_;
  int height_;

  void process();

  void updateTerminalSize();
  void printMessages();
  void printUserInput();
};

#endif