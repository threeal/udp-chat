#ifndef UDP_CHAT_UDP_H
#define UDP_CHAT_UDP_H

#include <list>
#include <string>
#include <thread>

class UDP
{
private:

  int socket_ = -1;
  int port_ = 9797;

  std::list<std::string> received_ = { };
  std::list<std::string> sent_ = { };

  std::thread *thread_ = nullptr;

public:

  UDP(int port);
  ~UDP();

  void process();

  void sendMessage(std::string message) { sent_.push_back(message); }

  bool isReceiveMessage() { return (received_.size() > 0); }
  std::string getReceivedMessage()
  {
    std::string received = received_.front();
    received_.pop_front();
    return received;
  }
};

#endif